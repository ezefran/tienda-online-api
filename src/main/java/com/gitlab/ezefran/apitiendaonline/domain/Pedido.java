package com.gitlab.ezefran.apitiendaonline.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "pedido_id")
    private List<ItemPedido> itemsPedido;

    private String nombre;

    private String apellido;

    private String localidad;

    private String direccion;

    private String telefono;

    private String comentario;

    @JsonFormat(pattern="dd/MM/yy HH:mm")
    private LocalDateTime fecha;

    @Column(precision = 9, scale = 2)
    private BigDecimal cantidad;

    @Column(precision = 9, scale = 2)
    private BigDecimal importe;

    @Enumerated(EnumType.STRING)
    private EstadoPedido estado;
}
