package com.gitlab.ezefran.apitiendaonline.domain;

public enum EstadoPedido {
    ABIERTO, EN_CURSO, CERRADO
}
