package com.gitlab.ezefran.apitiendaonline.domain;

public enum TipoProducto {
    PESO, UNIDAD
}
