package com.gitlab.ezefran.apitiendaonline.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemPedido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nombre;

    @Column(precision = 9, scale = 2)
    private BigDecimal precioUnidad;

    @Column(precision = 9, scale = 2)
    private BigDecimal cantidad;
}
