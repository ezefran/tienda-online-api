package com.gitlab.ezefran.apitiendaonline.domain;

public enum CategoriaProducto {
    VERDURA, FRUTA
}
