package com.gitlab.ezefran.apitiendaonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTiendaOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTiendaOnlineApplication.class, args);
	}

}
