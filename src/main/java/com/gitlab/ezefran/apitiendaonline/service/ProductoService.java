package com.gitlab.ezefran.apitiendaonline.service;

import com.gitlab.ezefran.apitiendaonline.domain.Producto;

import java.util.List;

public interface ProductoService {
    List<Producto> buscarTodos();
    void actualizar(int id, Producto producto);
    void guardar(Producto producto);
    void eliminar(int id);
}
