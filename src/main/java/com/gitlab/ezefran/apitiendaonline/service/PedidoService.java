package com.gitlab.ezefran.apitiendaonline.service;

import com.gitlab.ezefran.apitiendaonline.domain.EstadoPedido;
import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;
import com.gitlab.ezefran.apitiendaonline.domain.Pedido;

import java.util.List;

public interface PedidoService {
    List<Pedido> buscarTodos();
    List<ItemPedido> buscarProductosDelPedido(int id);
    void cambiarEstadoPedido(int id, EstadoPedido estado);
    void guardar(Pedido pedido);

}
