package com.gitlab.ezefran.apitiendaonline.service.impl;

import com.gitlab.ezefran.apitiendaonline.domain.Producto;
import com.gitlab.ezefran.apitiendaonline.repository.ProductoRepository;
import com.gitlab.ezefran.apitiendaonline.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Transactional
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    @Override
    public List<Producto> buscarTodos() {
        return productoRepository.findAll();
    }

    @Override
    public void actualizar(int id, Producto producto) {
        Optional<Producto> productoExistente = productoRepository.findById(id);

        if (productoExistente.isPresent()) {
            producto.setId(id);
            productoRepository.save(producto);
        } else {
            throw new IllegalArgumentException("El producto no existe");
        }
    }

    @Override
    public void guardar(Producto producto) {
        if (producto.getId() == null) {
            validarProducto(producto);
            productoRepository.save(producto);
        } else {
            throw new IllegalArgumentException("El producto no debe tener id");
        }
    }

    @Override
    public void eliminar(int id) {
        Optional<Producto> producto = productoRepository.findById(id);
        if (producto.isPresent()) {
            productoRepository.delete(producto.get());
        } else {
            throw new IllegalArgumentException("El producto no existe");
        }
    }

    private void validarProducto(Producto producto) {
        if (textoInvalido(producto.getNombre())) {
            throw new IllegalArgumentException("Datos del producto invalidos");
        }
    }

    private boolean textoInvalido(String nombre) {
        boolean regexMatch = Pattern
                .compile("^[a-zA-Z ]*$")
                .matcher(nombre)
                .results()
                .findFirst()
                .isPresent();

        return !regexMatch || nombre.length() > 50;
    }
}
