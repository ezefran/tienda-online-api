package com.gitlab.ezefran.apitiendaonline.service.impl;

import com.gitlab.ezefran.apitiendaonline.domain.EstadoPedido;
import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;
import com.gitlab.ezefran.apitiendaonline.domain.Pedido;
import com.gitlab.ezefran.apitiendaonline.repository.PedidoRepository;
import com.gitlab.ezefran.apitiendaonline.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Transactional
public class PedidoServiceImpl implements PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    @Override
    public List<Pedido> buscarTodos() {
        return pedidoRepository.findAll();
    }

    @Override
    public List<ItemPedido> buscarProductosDelPedido(int id) {
        Pedido pedido = pedidoRepository.findAll().stream()
                .filter(p -> p.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("El pedido no existe"));

        return pedido.getItemsPedido();
    }

    @Override
    public void cambiarEstadoPedido(int id, EstadoPedido estado) {
        Optional<Pedido> pedidoExistente = pedidoRepository.findById(id);

        if (pedidoExistente.isPresent()) {
            pedidoExistente.get().setEstado(estado);
        } else {
            throw new IllegalArgumentException("El pedido no existe");
        }
    }

    @Override
    public void guardar(Pedido pedido) {
        if (pedido.getId() == null) {
            validarPedido(pedido);
            pedidoRepository.save(pedido);
        } else {
            throw new IllegalArgumentException("El pedido no debe tener id");
        }
    }

    private void validarPedido(Pedido pedido) {
        if (textoInvalido(pedido.getNombre())
                || textoInvalido(pedido.getApellido())
                || textoInvalido(pedido.getLocalidad())
                || telefonoInvalido(pedido.getTelefono())
                || direccionInvalida(pedido.getDireccion())) {
            throw new IllegalArgumentException("Datos del pedido invalidos");
        }
    }

    private boolean textoInvalido(String nombre) {
        boolean regexMatch = Pattern
                .compile("^[a-zA-Z ]*$")
                .matcher(nombre)
                .results()
                .findFirst()
                .isPresent();

        return !regexMatch || nombre.length() > 50;
    }

    private boolean telefonoInvalido(String telefono) {
        boolean regexMatch = Pattern
                .compile("^(11|15)?[0-9]{8}$")
                .matcher(telefono)
                .results()
                .findFirst()
                .isPresent();

        return !regexMatch;
    }

    private boolean direccionInvalida(String direccion) {
        boolean regexMatch = Pattern
                .compile("^[a-zA-Z ]*( [0-9]*)?$")
                .matcher(direccion)
                .results()
                .findFirst()
                .isPresent();

        return !regexMatch;
    }
}
