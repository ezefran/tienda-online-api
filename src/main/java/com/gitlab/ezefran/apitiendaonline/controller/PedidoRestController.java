package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;
import com.gitlab.ezefran.apitiendaonline.domain.Pedido;
import com.gitlab.ezefran.apitiendaonline.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(path = "/api/pedidos")
public class PedidoRestController {

    @Autowired
    private PedidoService pedidoService;

    @GetMapping("/")
    public List<Pedido> buscarPedidos() {
        return pedidoService.buscarTodos();
    }

    @GetMapping("/{id}")
    public List<ItemPedido> buscarProductosDelPedido(@PathVariable("id") int id) {
        try {
            return pedidoService.buscarProductosDelPedido(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public void cambiarEstadoPedido(@PathVariable("id") int id, @RequestBody Pedido pedidoConNuevoEstado) {
        try {
            pedidoService.cambiarEstadoPedido(id, pedidoConNuevoEstado.getEstado());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/")
    public void guardarPedido(@RequestBody Pedido pedido) {
        try {
            pedidoService.guardar(pedido);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
