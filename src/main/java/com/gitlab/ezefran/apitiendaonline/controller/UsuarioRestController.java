package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.domain.Usuario;
import com.gitlab.ezefran.apitiendaonline.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/api/usuarios")
public class UsuarioRestController {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/auth")
    public String autenticar(@RequestBody Usuario usuario) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuario.getNombre(), usuario.getPassword()));
        } catch (BadCredentialsException | InternalAuthenticationServiceException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Credenciales incorrectas");
        }
        return jwtService.generateToken(usuario.getNombre());
    }

}
