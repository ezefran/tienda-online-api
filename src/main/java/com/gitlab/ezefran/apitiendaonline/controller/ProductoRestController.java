package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.domain.Producto;
import com.gitlab.ezefran.apitiendaonline.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(path = "/api/productos")
public class ProductoRestController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("/")
    public List<Producto> buscarProductos() {
        return productoService.buscarTodos();
    }

    @PutMapping("/{id}")
    public void actualizarProducto(@PathVariable("id") int id, @RequestBody Producto producto) {
        try {
            productoService.actualizar(id, producto);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/")
    public void guardarProducto(@RequestBody Producto producto) {
        try {
            productoService.guardar(producto);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void eliminarProducto(@PathVariable("id") int id) {
        try {
            productoService.eliminar(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
