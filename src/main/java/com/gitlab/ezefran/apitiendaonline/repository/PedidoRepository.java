package com.gitlab.ezefran.apitiendaonline.repository;

import com.gitlab.ezefran.apitiendaonline.domain.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PedidoRepository extends JpaRepository<Pedido, Integer> {
}
