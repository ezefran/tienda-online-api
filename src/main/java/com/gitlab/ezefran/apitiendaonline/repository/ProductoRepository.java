package com.gitlab.ezefran.apitiendaonline.repository;

import com.gitlab.ezefran.apitiendaonline.domain.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}
