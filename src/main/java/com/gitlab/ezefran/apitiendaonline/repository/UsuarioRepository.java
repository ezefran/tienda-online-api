package com.gitlab.ezefran.apitiendaonline.repository;

import com.gitlab.ezefran.apitiendaonline.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    Usuario findByNombre(String nombre);
}
