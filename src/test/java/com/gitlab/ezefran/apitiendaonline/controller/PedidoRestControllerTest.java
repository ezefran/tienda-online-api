package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.builder.ItemPedidoBuilder;
import com.gitlab.ezefran.apitiendaonline.builder.PedidoBuilder;
import com.gitlab.ezefran.apitiendaonline.domain.EstadoPedido;
import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;
import com.gitlab.ezefran.apitiendaonline.domain.Pedido;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class PedidoRestControllerTest {

    @Autowired
    private PedidoRestController pedidoRestController;

    @Test
    public void buscarPedidos_existenPedidos_retornaPedidos() {
        List<Pedido> pedidos = pedidoRestController.buscarPedidos();

        List<ItemPedido> itemsPedido = pedidos.stream()
                .flatMap(pedido -> pedido.getItemsPedido().stream())
                .collect(Collectors.toList());

        assertThat(pedidos).hasSize(3);
        assertThat(itemsPedido).hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    public void buscarProductosDelPedido_pedidoExistente_retornaProductos() {
        int pedidoId = 1;

        List<ItemPedido> productos = pedidoRestController.buscarProductosDelPedido(pedidoId);

        assertThat(productos).isNotEmpty();
    }

    @Test
    public void buscarProductosDelPedido_pedidoInexistente_lanzaExcepcion() {
        int pedidoId = 100;

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> pedidoRestController.buscarProductosDelPedido(pedidoId))
                .withMessage("404 NOT_FOUND \"El pedido no existe\"");
    }

    @Test
    public void cambiarEstadoPedido_pedidoExistente_cambiaEstado() {
        int pedidoId = 1;
        EstadoPedido nuevoEstado = EstadoPedido.CERRADO;

        Pedido pedidoConNuevoEstado = new PedidoBuilder().completarCamposPorDefecto()
                .estado(nuevoEstado)
                .build();

        pedidoRestController.cambiarEstadoPedido(pedidoId, pedidoConNuevoEstado);

        EstadoPedido estado = pedidoRestController.buscarPedidos().stream()
                .filter(p -> p.getId() == 1)
                .findFirst()
                .orElseThrow(IllegalStateException::new)
                .getEstado();

        assertThat(estado).isEqualTo(nuevoEstado);
    }

    @Test
    public void cambiarEstadoPedido_pedidoInexistente_lanzaExcepcion() {
        int pedidoId = 100;
        EstadoPedido nuevoEstado = EstadoPedido.CERRADO;

        Pedido pedidoConNuevoEstado = new PedidoBuilder().completarCamposPorDefecto()
                .estado(nuevoEstado)
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> pedidoRestController.cambiarEstadoPedido(pedidoId, pedidoConNuevoEstado))
                .withMessage("404 NOT_FOUND \"El pedido no existe\"");
    }

    @Test
    public void guardarPedido_datosValidos_guardaPedido() {
        Pedido pedido = new PedidoBuilder().completarCamposPorDefecto()
                .id(null)
                .build();
        ItemPedido itemPedido = new ItemPedidoBuilder().completarCamposPorDefecto()
                .id(null)
                .build();
        pedido.setItemsPedido(List.of(itemPedido));

        Assertions.assertDoesNotThrow(() -> pedidoRestController.guardarPedido(pedido));
    }

    @Test
    public void guardarPedido_datosInvalidos_lanzaExcepcion() {
        Pedido pedido = new PedidoBuilder().completarCamposPorDefecto()
                .id(null)
                .nombre("34247823942")
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> pedidoRestController.guardarPedido(pedido))
                .withMessage("400 BAD_REQUEST \"Datos del pedido invalidos\"");
    }

    @Test
    public void guardarPedido_pedidoConId_lanzaExcepcion() {
        Pedido pedido = new PedidoBuilder().completarCamposPorDefecto()
                .id(1)
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> pedidoRestController.guardarPedido(pedido))
                .withMessage("400 BAD_REQUEST \"El pedido no debe tener id\"");
    }
}
