package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.domain.Usuario;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class UsuarioRestControllerTest {

    @Autowired
    private UsuarioRestController usuarioRestController;

    @Test
    public void autenticar_usuarioValido_retornaToken() {
        Usuario usuario = new Usuario();
        usuario.setNombre("admin");
        usuario.setPassword("123");

        String token = usuarioRestController.autenticar(usuario);

        assertThat(token.length()).isEqualTo(152);
    }

    @Test
    public void autenticar_usuarioInexistente_lanzaExcepcion() {
        Usuario usuario = new Usuario();
        usuario.setNombre("noexisto");
        usuario.setPassword("123");

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> usuarioRestController.autenticar(usuario))
                .withMessage("401 UNAUTHORIZED \"Credenciales incorrectas\"");
    }

    @Test
    public void autenticar_passwordIncorrecta_lanzaExcepcion() {
        Usuario usuario = new Usuario();
        usuario.setNombre("admin");
        usuario.setPassword("1234");

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> usuarioRestController.autenticar(usuario))
                .withMessage("401 UNAUTHORIZED \"Credenciales incorrectas\"");
    }
}
