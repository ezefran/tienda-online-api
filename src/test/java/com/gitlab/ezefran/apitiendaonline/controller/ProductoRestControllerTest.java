package com.gitlab.ezefran.apitiendaonline.controller;

import com.gitlab.ezefran.apitiendaonline.builder.ProductoBuilder;
import com.gitlab.ezefran.apitiendaonline.domain.Producto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class ProductoRestControllerTest {

    @Autowired
    private ProductoRestController productoRestController;

    @Test
    public void buscarProductos_existenProductos_retornaProductos() {
        List<Producto> productos = productoRestController.buscarProductos();

        assertThat(productos).hasSize(3);
    }

    @Test
    public void actualizarProducto_productoExistente_actualizaProducto() {
        int productoId = 1;
        Producto productoActualizado = new ProductoBuilder().completarCamposPorDefecto()
                .id(productoId)
                .build();

        productoRestController.actualizarProducto(productoId, productoActualizado);

        String nombre = productoRestController.buscarProductos().stream()
                .filter(p -> p.getId() == productoId)
                .findFirst()
                .orElseThrow(IllegalStateException::new)
                .getNombre();

        assertThat(nombre).isEqualTo(productoActualizado.getNombre());
    }

    @Test
    public void actualizarProducto_productoInexistente_lanzaExcepcion() {
        int productoId = 100;
        Producto productoActualizado = new ProductoBuilder().completarCamposPorDefecto()
                .id(productoId)
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productoRestController.actualizarProducto(productoId, productoActualizado))
                .withMessage("404 NOT_FOUND \"El producto no existe\"");
    }

    @Test
    public void guardarProducto_datosValidos_guardaProducto() {
        Producto producto = new ProductoBuilder().completarCamposPorDefecto()
                .id(null)
                .build();

        Assertions.assertDoesNotThrow(() -> productoRestController.guardarProducto(producto));
    }

    @Test
    public void guardarProducto_datosInvalidos_lanzaExcepcion() {
        Producto producto = new ProductoBuilder().completarCamposPorDefecto()
                .id(null)
                .nombre("34247823942")
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productoRestController.guardarProducto(producto))
                .withMessage("400 BAD_REQUEST \"Datos del producto invalidos\"");
    }

    @Test
    public void guardarProducto_productoConId_lanzaExcepcion() {
        Producto producto = new ProductoBuilder().completarCamposPorDefecto()
                .id(1)
                .build();

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productoRestController.guardarProducto(producto))
                .withMessage("400 BAD_REQUEST \"El producto no debe tener id\"");
    }

    @Test
    public void eliminarProducto_productoExistente_eliminaProducto() {
        int productoId = 1;

        productoRestController.eliminarProducto(productoId);

        Optional<Producto> producto = productoRestController.buscarProductos().stream()
                .filter(p -> p.getId() == productoId)
                .findFirst();

        assertThat(producto.isPresent()).isFalse();
    }

    @Test
    public void eliminarProducto_productoInexistente_lanzaExcepcion() {
        int productoId = 100;

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productoRestController.eliminarProducto(productoId))
                .withMessage("404 NOT_FOUND \"El producto no existe\"");
    }
}
