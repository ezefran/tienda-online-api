package com.gitlab.ezefran.apitiendaonline.builder;

import com.gitlab.ezefran.apitiendaonline.domain.CategoriaProducto;
import com.gitlab.ezefran.apitiendaonline.domain.Producto;
import com.gitlab.ezefran.apitiendaonline.domain.TipoProducto;

import java.math.BigDecimal;

public class ProductoBuilder {

    private Producto producto;

    public ProductoBuilder() {
        producto = new Producto();
    }

    public ProductoBuilder id(Integer id) {
        producto.setId(id);
        return this;
    }

    public ProductoBuilder nombre(String nombre) {
        producto.setNombre(nombre);
        return this;
    }

    public ProductoBuilder nombreImagen(String nombreImagen) {
        producto.setNombreImagen(nombreImagen);
        return this;
    }

    public ProductoBuilder categoria(CategoriaProducto categoria) {
        producto.setCategoria(categoria);
        return this;
    }

    public ProductoBuilder tipo(TipoProducto tipo) {
        producto.setTipo(tipo);
        return this;
    }

    public ProductoBuilder precioUnidad(BigDecimal precioUnidad) {
        producto.setPrecioUnidad(precioUnidad);
        return this;
    }

    public ProductoBuilder completarCamposPorDefecto() {
        producto.setId(100);
        producto.setNombre("nombre");
        producto.setNombreImagen("nombre.png");
        producto.setCategoria(CategoriaProducto.FRUTA);
        producto.setTipo(TipoProducto.PESO);
        producto.setPrecioUnidad(BigDecimal.valueOf(10));
        return this;
    }

    public Producto build() {
        return producto;
    }
}
