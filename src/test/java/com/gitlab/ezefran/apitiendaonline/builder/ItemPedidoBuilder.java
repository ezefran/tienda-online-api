package com.gitlab.ezefran.apitiendaonline.builder;

import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;

import java.math.BigDecimal;

public class ItemPedidoBuilder {

    private ItemPedido itemPedido;

    public ItemPedidoBuilder() {
        itemPedido = new ItemPedido();
    }

    public ItemPedidoBuilder id(Integer id) {
        itemPedido.setId(id);
        return this;
    }

    public ItemPedidoBuilder nombre(String nombre) {
        itemPedido.setNombre(nombre);
        return this;
    }

    public ItemPedidoBuilder cantidad(BigDecimal cantidad) {
        itemPedido.setCantidad(cantidad);
        return this;
    }

    public ItemPedidoBuilder precioUnidad(BigDecimal precioUnidad) {
        itemPedido.setPrecioUnidad(precioUnidad);
        return this;
    }

    public ItemPedidoBuilder completarCamposPorDefecto() {
        itemPedido.setId(100);
        itemPedido.setNombre("nombre");
        itemPedido.setPrecioUnidad(BigDecimal.valueOf(10));
        itemPedido.setCantidad(BigDecimal.valueOf(1));
        return this;
    }

    public ItemPedido build() {
        return itemPedido;
    }
}
