package com.gitlab.ezefran.apitiendaonline.builder;

import com.gitlab.ezefran.apitiendaonline.domain.EstadoPedido;
import com.gitlab.ezefran.apitiendaonline.domain.ItemPedido;
import com.gitlab.ezefran.apitiendaonline.domain.Pedido;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class PedidoBuilder {

    private Pedido pedido;

    public PedidoBuilder() {
        pedido = new Pedido();
    }

    public PedidoBuilder id(Integer id) {
        pedido.setId(id);
        return this;
    }

    public PedidoBuilder items(List<ItemPedido> items) {
        pedido.setItemsPedido(items);
        return this;
    }

    public PedidoBuilder nombre(String nombre) {
        pedido.setNombre(nombre);
        return this;
    }

    public PedidoBuilder apellido(String apellido) {
        pedido.setApellido(apellido);
        return this;
    }

    public PedidoBuilder localidad(String localidad) {
        pedido.setLocalidad(localidad);
        return this;
    }

    public PedidoBuilder direccion(String direccion) {
        pedido.setDireccion(direccion);
        return this;
    }

    public PedidoBuilder telefono(String telefono) {
        pedido.setTelefono(telefono);
        return this;
    }

    public PedidoBuilder comentario(String comentario) {
        pedido.setComentario(comentario);
        return this;
    }

    public PedidoBuilder fecha(LocalDateTime fecha) {
        pedido.setFecha(fecha);
        return this;
    }

    public PedidoBuilder cantidad(BigDecimal cantidad) {
        pedido.setCantidad(cantidad);
        return this;
    }

    public PedidoBuilder importe(BigDecimal importe) {
        pedido.setImporte(importe);
        return this;
    }

    public PedidoBuilder estado(EstadoPedido estado) {
        pedido.setEstado(estado);
        return this;
    }

    public PedidoBuilder completarCamposPorDefecto() {
        pedido.setId(100);
        pedido.setNombre("nombre");
        pedido.setApellido("apellido");
        pedido.setLocalidad("localidad");
        pedido.setDireccion("direccion 123");
        pedido.setTelefono("12345678");
        pedido.setComentario("");
        pedido.setFecha(LocalDateTime.now());
        pedido.setCantidad(BigDecimal.valueOf(2));
        pedido.setImporte(BigDecimal.valueOf(20));
        pedido.setEstado(EstadoPedido.ABIERTO);
        return this;
    }

    public Pedido build() {
        return pedido;
    }
}
