INSERT INTO producto (nombre, tipo, categoria, precio_unidad, nombre_imagen) VALUES
('Manzana', 'PESO', 'FRUTA', '50', 'manzana.png'),
('Lechuga', 'PESO', 'VERDURA', '100', 'lechuga.png'),
('Naranja', 'PESO', 'FRUTA', '50', 'naranja.png');

INSERT INTO pedido (nombre, apellido, localidad, direccion, telefono, comentario, fecha, cantidad, importe, estado) VALUES
('Pepe', 'Lopez', 'localidad falsa', 'calle falsa 123', '34343434', '', '2020-10-01 14:30', '1', '50', 'ABIERTO'),
('Jorge', 'Lopez', 'localidad falsa', 'calle falsa 123', '34343434', '', '2019-12-01 14:30', '2', '200', 'EN_CURSO'),
('Jorge', 'Perez', 'localidad falsa', 'calle falsa 123', '34343434', '', '2019-11-11 14:30', '2', '100', 'CERRADO');

INSERT INTO item_pedido (pedido_id, nombre, precio_unidad, cantidad) VALUES
('1', 'Manzana', '50', '1'),
('2', 'Lechuga', '100', '2'),
('3', 'Naranja', '50' ,'2');

INSERT INTO usuario (nombre, password) VALUES
('admin', '$2y$12$ZWnBXRFWq/sQGsK3Z/42IegJUZ6o72CuAht8rrBdWSF6R/qHpTLoy');